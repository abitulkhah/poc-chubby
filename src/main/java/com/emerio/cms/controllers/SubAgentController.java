package com.emerio.cms.controllers;

import com.emerio.cms.dto.SubAgentDto;
import com.emerio.cms.model.Agent;
import com.emerio.cms.model.SubAgent;
import com.emerio.cms.repositories.AgentRepository;
import com.emerio.cms.repositories.SubAgentRepository;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/v1/subagent")
public class SubAgentController {

    @Autowired
    private SubAgentRepository subAgentRepository;

    @Autowired
    private AgentRepository agentRepository;

    @Autowired
    private ApplicationContext context;

    private ModelMapper modelMapper = new ModelMapper();

    @GetMapping
    public ResponseEntity<JSONObject> listAgent(){

        JSONObject jsonObject = new JSONObject();

        try{

            List<SubAgent> subAgentList = subAgentRepository.findAll();
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All SubAgent");
            jsonObject.put("data", subAgentList);

        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);

    }

    @PostMapping
    public ResponseEntity<JSONObject> create(@RequestBody SubAgentDto subAgentDto) throws IOException {

        JSONObject jsonObject = new JSONObject();

        try{

            SubAgent subAgent = modelMapper.map(subAgentDto, SubAgent.class);
            Agent agent = agentRepository.findById(subAgentDto.getAgentCode()).get();
            subAgent.setAgent(agent);
            subAgentRepository.save(subAgent);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create SubAgent");
        }catch (Exception e){
                jsonObject.put("message", e.toString());

        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> update(@RequestBody SubAgentDto subAgentDto) throws IOException {

        JSONObject jsonObject = new JSONObject();

        try{

            SubAgent subAgent = subAgentRepository.findById(subAgentDto.getId()).get();

            subAgent.setSubAgentCode(subAgentDto.getSubAgentCode() != null ? subAgentDto.getSubAgentCode() : subAgent.getSubAgentCode());
            subAgent.setDescription(subAgentDto.getDescription() != null ? subAgentDto.getDescription() : subAgent.getDescription());

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Update SubAgent");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @DeleteMapping(path="/{id}")
    public ResponseEntity<JSONObject> delete(@PathVariable(value = "id") Long id){

        JSONObject jsonObject = new JSONObject();

        try{

            subAgentRepository.deleteById(id);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Delete SubAgent");

        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);

    }
}
