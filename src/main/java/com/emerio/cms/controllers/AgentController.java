package com.emerio.cms.controllers;

import com.emerio.cms.dto.AgentDto;
import com.emerio.cms.model.Agent;
import com.emerio.cms.repositories.AgentRepository;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/v1/agent")
public class AgentController {

    @Autowired
    private AgentRepository agentsRepository;

    @Autowired
    private ApplicationContext context;

    private ModelMapper modelMapper = new ModelMapper();

    @GetMapping
    public ResponseEntity<JSONObject> listAgent(){

        JSONObject jsonObject = new JSONObject();

        try{

            List<Agent> agentList = agentsRepository.findAll();
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Agent");
            jsonObject.put("data", agentList);

        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);

    }

    @PostMapping
    public ResponseEntity<JSONObject> create(@RequestBody AgentDto agentDto) throws IOException {

        JSONObject jsonObject = new JSONObject();

        try{

            Agent agent = modelMapper.map(agentDto, Agent.class);
            agentsRepository.save(agent);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Agent");
        }catch (Exception e){
            if(e.toString().contains("constraint")){
                jsonObject.put("message", "Agentname atau email sudah terdaftar");
            }else{
                jsonObject.put("message", e.toString());

            }
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> update(@RequestBody AgentDto agentDto) throws IOException {

        JSONObject jsonObject = new JSONObject();

        try{

            Agent agent = agentsRepository.findById(agentDto.getId()).get();

            agent.setAgentCode(agentDto.getAgentCode() != null ? agentDto.getAgentCode() : agent.getAgentCode());
            agent.setDescription(agentDto.getDescription() != null ? agentDto.getDescription() : agent.getDescription());

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Update Agent");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @DeleteMapping(path="/{id}")
    public ResponseEntity<JSONObject> delete(@PathVariable(value = "id") Long id){

        JSONObject jsonObject = new JSONObject();

        try{

            agentsRepository.deleteById(id);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Delete Agent");

        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);

    }

}
