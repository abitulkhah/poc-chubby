package com.emerio.cms.controllers;

import com.emerio.cms.dto.RoleDto;
import com.emerio.cms.model.Role;
import com.emerio.cms.repositories.RoleRepository;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/v1/role")
public class RoleController {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private ApplicationContext context;

    @GetMapping
    public ResponseEntity<JSONObject> listRole(){

        JSONObject jsonObject = new JSONObject();

        try{


            List<Role> roleList = roleRepository.findAll();
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Role");
            jsonObject.put("data", roleList);

        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);

    }

    @PostMapping
    public ResponseEntity<JSONObject> create(@RequestBody RoleDto roleDto) throws IOException {

        JSONObject jsonObject = new JSONObject();

        try{

            Role role = modelMapper.map(roleDto, Role.class);
            roleRepository.save(role);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Role");
        }catch (Exception e){
            if(e.toString().contains("constraint")){
                jsonObject.put("message", "Rolename atau email sudah terdaftar");
            }else{
                jsonObject.put("message", e.toString());

            }
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> update(@RequestBody RoleDto roleDto) throws IOException {

        JSONObject jsonObject = new JSONObject();

        try{

            Role role = roleRepository.findById(roleDto.getId()).get();

            role.setRoleName(roleDto.getRoleName() != null ? roleDto.getRoleName() : role.getRoleName());
            role.setDescription(roleDto.getDescription() != null ? roleDto.getDescription() : role.getDescription());

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Update Role");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @DeleteMapping(path="/{id}")
    public ResponseEntity<JSONObject> delete(@PathVariable(value = "id") Long id){

        JSONObject jsonObject = new JSONObject();

        try{

            roleRepository.deleteById(id);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Delete Role");

        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);

    }
}
