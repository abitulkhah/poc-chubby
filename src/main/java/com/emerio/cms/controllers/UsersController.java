package com.emerio.cms.controllers;

import com.emerio.cms.dto.UserDto;
import com.emerio.cms.model.*;
import com.emerio.cms.repositories.AgentRepository;
import com.emerio.cms.repositories.RoleRepository;
import com.emerio.cms.repositories.UsersRepository;
import com.emerio.cms.repositories.UsersRoleRepository;
import com.emerio.cms.services.UserService;
import com.emerio.cms.utils.ImageEncoder;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/v1/users")
public class UsersController {

    @Autowired
    private UserService userService;

    @Autowired
    private UsersRoleRepository userRoleRepository;

    @Autowired
    private AgentRepository agentRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ImageEncoder imageEncoder;

    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private ApplicationContext context;

    @GetMapping
    public ResponseEntity<JSONObject> listUser(){

        JSONObject jsonObject = new JSONObject();

        try{

            Role role =  roleRepository.findById(Long.valueOf(1)).get();

            List<UsersRole> userRoleList = userRoleRepository.findAllByRole(role);
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All User");
            jsonObject.put("data", userRoleList);

        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);

    }

    @PostMapping
    public ResponseEntity<JSONObject> create(@RequestBody UserDto userDto) throws IOException {

        JSONObject jsonObject = new JSONObject();

        try{

            Users users = modelMapper.map(userDto, Users.class);
            users.setPassword(passwordEncoder.encode(userDto.getPassword()));

            if(userDto.getBase64Image()!=null){
                users.setBase64Image(imageEncoder.base64Decode(userDto.getBase64Image(), userDto.getImageName(),"image"));
            }

            userService.save(users);
            UsersRole userRole = new UsersRole();
            Role role = roleRepository.findById(userDto.getRoleId()).get();
            userRole.setRole(role);
            userRole.setUsers(users);
            userRoleRepository.save(userRole);

            Agent agent = agentRepository.findByAgentCode(userDto.getAgentType());

            UsersAgent usersAgent = new UsersAgent();
            usersAgent.setAgent(agent);
            usersAgent.setUsers(users);


            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create User");
        }catch (Exception e){
            if(e.toString().contains("constraint")){
                jsonObject.put("message", "Username atau email sudah terdaftar");
            }else{
                jsonObject.put("message", e.toString());

            }
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> update(@RequestBody UserDto userDto) throws IOException {

        JSONObject jsonObject = new JSONObject();

        try{

            Users users = usersRepository.findById(userDto.getId()).get();

            users.setBase64Image(userDto.getBase64Image() !=null ? imageEncoder.base64Decode(userDto.getBase64Image(), userDto.getImageName(),"image") : users.getBase64Image());
            users.setEmail(userDto.getEmail() !=null ? userDto.getEmail() : users.getEmail());
            users.setFullName(userDto.getFullName() !=null ? userDto.getFullName() : users.getFullName());
            users.setMimeType(userDto.getMimeType() !=null ? userDto.getMimeType() : users.getMimeType());
            users.setPassword(userDto.getPassword() !=null ?  passwordEncoder.encode(userDto.getPassword()) : passwordEncoder.encode(users.getPassword()));
            users.setPhoneNumber(userDto.getPhoneNumber() !=null ? userDto.getPhoneNumber() : users.getPhoneNumber());
            users.setUsername(userDto.getUsername() !=null ? userDto.getUsername() : users.getUsername());

            userService.save(users);
            UsersRole userRole = new UsersRole();
            Role role = roleRepository.findById(userDto.getRoleId()).get();
            userRole.setRole(role);
            userRole.setUsers(users);
            userRoleRepository.save(userRole);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Update User");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @DeleteMapping(path="/{id}")
    public ResponseEntity<JSONObject> delete(@PathVariable(value = "id") Long id){

        JSONObject jsonObject = new JSONObject();

        try{

            userService.delete(id);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Delete User");

        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);

    }

}