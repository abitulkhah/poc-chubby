package com.emerio.cms.controllers;

import com.emerio.cms.dto.ApplicationSettingDto;
import com.emerio.cms.model.ApplicationSetting;
import com.emerio.cms.repositories.ApplicationSettingRepository;
import com.emerio.cms.utils.ImageEncoder;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/v1/settingApplication")
public class ApplicationSettingController {

    @Autowired
    private ApplicationSettingRepository applicationSettingRepository;

    @Autowired
    private ImageEncoder imageEncoder;

    private ModelMapper modelMapper = new ModelMapper();

    @GetMapping
    public ResponseEntity<JSONObject> getList(){

        JSONObject jsonObject = new JSONObject();

        try{

            List<ApplicationSetting> applicationSettingList = applicationSettingRepository.findAll();

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All ApplicationSetting");
            jsonObject.put("data", applicationSettingList);

        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);

    }

    @PostMapping
    public ResponseEntity<JSONObject> create(@RequestBody ApplicationSettingDto applicationSettingDto) throws IOException {

        JSONObject jsonObject = new JSONObject();

        try{

            ApplicationSetting applicationSetting = modelMapper.map(applicationSettingDto, ApplicationSetting.class);

            applicationSettingRepository.save(applicationSetting);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create ApplicationSetting");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> update(@RequestBody ApplicationSettingDto applicationSettingDto) throws IOException {

        JSONObject jsonObject = new JSONObject();

        try{

            ApplicationSetting applicationSetting = applicationSettingRepository.findById(applicationSettingDto.getId()).get();

            applicationSetting.setSettingName(applicationSettingDto.getSettingName() !=null ? applicationSettingDto.getSettingName() : applicationSetting.getSettingName());
            applicationSetting.setSettingCode(applicationSettingDto.getSettingCode() !=null ? applicationSettingDto.getSettingCode() : applicationSetting.getSettingCode());
            applicationSetting.setType(applicationSettingDto.getType() !=null ? applicationSettingDto.getType() : applicationSetting.getType());
            applicationSetting.setValue(applicationSettingDto.getValue() !=null ? applicationSettingDto.getValue() : applicationSetting.getValue());
            applicationSetting.setDescription(applicationSettingDto.getDescription() !=null ? applicationSettingDto.getDescription() : applicationSetting.getDescription());

            applicationSettingRepository.save(applicationSetting);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Update ApplicationSetting");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @DeleteMapping(path="/{id}")
    public ResponseEntity<JSONObject> delete(@PathVariable(value = "id") Long id){

        JSONObject jsonObject = new JSONObject();

        try{

            applicationSettingRepository.deleteById(id);

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Delete ApplicationSetting");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);

    }

}
