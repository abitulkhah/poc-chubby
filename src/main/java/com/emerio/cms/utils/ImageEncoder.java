package com.emerio.cms.utils;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;

@Service

public class ImageEncoder {

    @Autowired
    private ApplicationContext context;


    public String base64Encode(String imageName, String type) throws IOException {

        String imagePath = type=="image" ? context.getEnvironment().getProperty("image.path") :  context.getEnvironment().getProperty("video.path");

        byte[] fileContent = FileUtils.readFileToByteArray(new File(imagePath+"/"+imageName));
        String encodedString = Base64.getEncoder().encodeToString(fileContent);

        return encodedString;

    }

    public String base64Decode(String encodedString, String fileName, String type) throws IOException {

        String imagePath = type=="image" ? context.getEnvironment().getProperty("image.path") :  context.getEnvironment().getProperty("video.path");

        Long timeStamp = new Date().getTime();
        byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
        File file = new File(imagePath+"/"+timeStamp.toString()+"_"+fileName);

        if(file.exists()){
            file.delete();
        }


        FileUtils.writeByteArrayToFile(file, decodedBytes);

        return timeStamp.toString()+"_"+fileName;

    }

}
