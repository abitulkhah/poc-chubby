package com.emerio.cms.services;

import com.emerio.cms.model.Users;

import java.util.List;

public interface UserService {

    Users save(Users user);

    List<Users> findAll();
    void delete(long id);

}
