package com.emerio.cms.repositories;

import com.emerio.cms.model.SubAgent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubAgentRepository extends JpaRepository<SubAgent, Long> {
}
