package com.emerio.cms.repositories;

import com.emerio.cms.model.ApplicationSetting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationSettingRepository extends JpaRepository<ApplicationSetting, Long> {
}
