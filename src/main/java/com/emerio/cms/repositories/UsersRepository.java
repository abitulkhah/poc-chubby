package com.emerio.cms.repositories;

import com.emerio.cms.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository  extends JpaRepository<Users, Long> {

}
