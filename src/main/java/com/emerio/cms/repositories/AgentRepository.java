package com.emerio.cms.repositories;

import com.emerio.cms.model.Agent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AgentRepository extends JpaRepository<Agent, Long> {

    Agent findByAgentCode(String agentCode);

}
