package com.emerio.cms.repositories;

import com.emerio.cms.model.Role;
import com.emerio.cms.model.UsersRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsersRoleRepository extends JpaRepository<UsersRole, Long> {
    List<UsersRole> findAllByRole(Role role);

}

