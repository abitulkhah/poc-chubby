package com.emerio.cms.dto;

public class SubAgentDto {

    private Long id;

    private Long agentCode;

    private String subAgentCode;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(Long agentCode) {
        this.agentCode = agentCode;
    }

    public String getSubAgentCode() {
        return subAgentCode;
    }

    public void setSubAgentCode(String subAgentCode) {
        this.subAgentCode = subAgentCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
