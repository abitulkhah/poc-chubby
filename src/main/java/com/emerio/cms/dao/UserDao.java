package com.emerio.cms.dao;

import com.emerio.cms.model.Users;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<Users, Long> {
    Users findByUsername(String username);
}
