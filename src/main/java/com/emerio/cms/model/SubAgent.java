package com.emerio.cms.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "sub_agent")
public class SubAgent {

    @Id
    @GeneratedValue(generator = "sub_agent_generator")
    @SequenceGenerator(
            name = "sub_agent_generator",
            sequenceName = "sub_agent_sequence",
            initialValue = 10
    )
    private Long id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "agent_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Agent agent;

    @Column(name="sub_agent_code", nullable = false)
    private String subAgentCode;

    @Column(name="description", nullable = false)
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getSubAgentCode() {
        return subAgentCode;
    }

    public void setSubAgentCode(String subAgentCode) {
        this.subAgentCode = subAgentCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
