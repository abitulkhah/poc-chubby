package com.emerio.cms.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "users_agent")
public class UsersAgent extends AuditModel {

    @Id
    @GeneratedValue(generator = "users_agent_generator")
    @SequenceGenerator(
            name = "users_agent_generator",
            sequenceName = "users_agent_sequence",
            initialValue = 10
    )
    private Long id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "users_id", nullable = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Users users;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "agent_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Agent agent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }
}
