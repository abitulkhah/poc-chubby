package com.emerio.cms.model;

import javax.persistence.*;

@Entity
@Table(name = "document")
public class Document extends AuditModel{

    @Id
    @GeneratedValue(generator = "document_generator")
    @SequenceGenerator(
            name = "document_generator",
            sequenceName = "document_sequence",
            initialValue = 10
    )
    private Long id;

    @Column(name = "document_name")
    private String documentName;

    @Column(name = "document")
    private String document;

    @Column(name = "mime_type")
    private String mimeType;

    @Column(name = "description")
    private String description;

    @Column(name = "document_size")
    private Integer documentSize;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDocumentSize() {
        return documentSize;
    }

    public void setDocumentSize(Integer documentSize) {
        this.documentSize = documentSize;
    }
}
