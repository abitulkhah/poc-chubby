package com.emerio.cms.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "agent")
public class Agent {

    @Id
    @GeneratedValue(generator = "agent_generator")
    @SequenceGenerator(
            name = "agent_generator",
            sequenceName = "agent_sequence",
            initialValue = 10
    )
    private Long id;

    @Column(name="agent_code", nullable = false)
    private String agentCode;

    @Column(name="description", nullable = false)
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
