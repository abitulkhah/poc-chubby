package com.emerio.cms.model;


import javax.persistence.*;

@Entity
@Table(name = "role")
public class Role  extends AuditModel{

    @Id
    @GeneratedValue(generator = "role_generator")
    @SequenceGenerator(
            name = "role_generator",
            sequenceName = "role_sequence",
            initialValue = 10
    )
    private Long id;

    @Column(name="role_name", nullable = false)
    private String roleName;

    @Column(name="description", nullable = false)
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
