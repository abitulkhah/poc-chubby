package com.emerio.cms.model;

import javax.persistence.*;

@Entity
@Table(name = "application_setting")
public class ApplicationSetting extends AuditModel {

    @Id
    @GeneratedValue(generator = "application_setting_generator")
    @SequenceGenerator(
            name = "application_setting_generator",
            sequenceName = "application_setting_sequence",
            initialValue = 10
    )
    private Long id;

    @Column(name = "setting_name")
    private String settingName;

    @Column(name = "type")
    private String type;

    @Column(name = "setting_code")
    private String settingCode;

    @Column(name = "value")
    private String value;

    @Column(name = "description", columnDefinition = "text")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSettingName() {
        return settingName;
    }

    public void setSettingName(String settingName) {
        this.settingName = settingName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSettingCode() {
        return settingCode;
    }

    public void setSettingCode(String settingCode) {
        this.settingCode = settingCode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
